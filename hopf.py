#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pygame
import time
import math

WIDTH = 640
HEIGHT = 640

TIME_STEP = 0.01 # 10ms
WHITE = (255, 255, 255)

x = 1.
y = 0.
omega = 2
epsilon = 1.

pygame.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))
t_prev = time.clock()

ball = pygame.image.load("ball.jpg").convert()
transColor = ball.get_at((0,0)) # vire le fond
ball.set_colorkey(transColor)
ball = pygame.transform.scale(ball, (50, 40))

done = False
while not done:
    event = pygame.event.poll()
    if event.type == pygame.MOUSEMOTION:
       f, _ = event.pos
       F = (f - WIDTH/2.) / WIDTH
    elif event.type == pygame.QUIT:
        pygame.quit()
        done = True
        break
    
    #time.sleep(TIME_STEP) # wait for 10ms
    t = time.clock() # time in seconds
    dt = t - t_prev
    t_prev = t

    screen.fill(WHITE)

    # F = math.cos(3*t)

    # Hopf equations
    r = math.sqrt(x*x + y*y)
    xd = (1 - r*r)*x - omega*y + epsilon*F
    yd = (1 - r*r)*y + omega*x

    # learning rule
    omegad = -epsilon*F*(y / r)

    # update x, y, omega with derivatives
    x += xd * dt
    y += yd * dt
    omega += omegad * dt

    # update screen
    screen.blit(ball, (f, 50)) # sujet
    screen.blit(ball, (WIDTH/2 + x*0.9*WIDTH/2 - 30, 100)) # model
    pygame.display.update()

