#!/usr/bin/env python
# -*- coding: utf-8 -*-
# py -m pip install qqch

import pygame
import time
import math
import numpy as np
import matplotlib.pyplot as plt # pour jouer avant expé
from scipy.signal import hilbert# pour jouer avant expé

WIDTH = 640
HEIGHT = 640

TIME_STEP = 0.01 # 10ms
WHITE = (255, 255, 255)
RED = (255,10,10)
GREEN = (0,255,0)

# -- parametres du modele ----------------------------------------------------------- 
x = 1. # lie a l'amplitude
y = 0.
omega = 2 #fréquence intrinseque du modele
epsilon = 4. #the higher epsilon is, the faster the learning
F = 0. # driving signal 
f = 0.
data=[] # est une liste

# --- functions pour visualiser les caracteristiques du signal avant expé ---------------------------------------

def Mon_signal():
    D=np.asarray(data) # list en array
    HilM = hilbert(D[:,1]) # analyse du signal
    HilS = hilbert(D[:,2])
    InstPhaseM=np.unwrap(np.angle(HilM))
    InstPhaseS=np.unwrap(np.angle(HilS))
    PR=((InstPhaseM-InstPhaseS)*180)/math.pi # phase relative

    plt.subplot(211)
    p1 = plt.scatter(D[:,0],D[:,1], c = 'green', s = 2)
    p2 = plt.scatter(D[:,0],D[:,2], c = 'red', s = 2)
    plt.legend([p1, p2], ['model', 'sujet'])
    plt.title('position du modete et du sujet')

    plt.subplot(212)
    plt.scatter(D[:,0],(PR), c = 'blue', s = 2)
    plt.title('phase relative: modèle - sujet')
        
    plt.show()
    print('nia')
    return

# start de game -------------------------------------------------------
pygame.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))
t_prev = time.clock()

#ball = pygame.image.load("ball.jpg").convert()
#transColor = ball.get_at((0,0)) # vire le fond
#ball.set_colorkey(transColor)
#ball = pygame.transform.scale(ball, (50, 40))

done = False
while not done:
    event = pygame.event.poll()
    if event.type == pygame.MOUSEMOTION:
       f, _ = event.pos
       F = (f - WIDTH/2.) / WIDTH
    elif event.type == pygame.QUIT:    # Checks if the red button in the corner of the window is clicked
        np.savetxt('Hopf.txt', data ,delimiter='\t', newline='\n') # save the data '\r\n'
        Mon_signal() # visualise et analyse le signal 
        pygame.quit()
        done = True
        break
    
    #time.sleep(TIME_STEP) # wait for 10ms
    t = time.clock() # time in seconds
    dt = t - t_prev
    t_prev = t

    screen.fill(WHITE)

    # F = math.cos(3*t)

    # Hopf equations
    r = math.sqrt(x*x + y*y)
    xd = (1 - r*r)*x - omega*y + epsilon*F
    yd = (1 - r*r)*y + omega*x

    # learning rule
    omegad = -epsilon*F*(y / r)

    # update x, y, omega with derivatives
    x += xd * dt
    y += yd * dt
    omega += omegad * dt

    # update screen
    #screen.blit(ball, (f, 50)) # sujet
    #screen.blit(ball, (WIDTH/2 + x*0.9*WIDTH/2 - 30, 100)) # model
    pygame.draw.circle(screen, RED, (int(f),200), 15, 0)
    pygame.draw.circle(screen, GREEN, (int(WIDTH/2 + x*0.5*WIDTH/2 - 30),300), 15, 0)
    pygame.display.update()

    # print results on screen
    print(t, " ", WIDTH/2 + x*0.9*WIDTH/2 - 30, " ", f)

    # save data ds list 
    data.append([t,WIDTH/2 + x*0.5*WIDTH/2 - 30, f]) # time, model, sujet




    



 


    
